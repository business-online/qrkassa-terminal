module.exports = {
  'publicPath': process.env.NODE_ENV === 'production' ? '/betaalterminal/' : '/',
  'assetsDir': 'static',
  'transpileDependencies': [
    'vuetify'
  ],
  'pwa': {
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: './src/sw.js',
      swDest: 'service-worker.js'
    },
    manifestOptions:
      {
        name: 'QR Kassa Betaalterminal',
        short_name: 'qr kassa',
        theme_color: '#2ab0ed',
        start_url: '/betaalterminal/index.html'
      }
  }
}
