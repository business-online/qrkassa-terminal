import axios from 'axios'

const API_URL = 'https://backoffice.qr-kassa.nl/api'
// const API_URL = 'https://e-qrnl.localhost/api'

export default axios.create({
  baseURL: API_URL
})
