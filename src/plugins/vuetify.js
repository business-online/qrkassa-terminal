import Vue from 'vue'
import Vuetify from 'vuetify/lib'
// Vue.use(Vuetify)

Vue.use(Vuetify, {
  iconfont: 'fa' // 'md' || 'mdi' || 'fa' || 'fa4'
})

export default new Vuetify({})
