import Vue from 'vue'
import Vuex from 'vuex'

import VuexPersist from 'vuex-persist/dist/umd'

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: 'e-qr',
  storage: localStorage
})

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  state: {
    authKey: '',
    accessToken: '',
    terminal: '',
    terminalId: '',
    transactionDescription: '',
    transactionDescriptionEditable: '',
    organizationId: '',
    organization: '',
    compactMode: false
  },
  mutations: {
    addAuthKey (state, value) {
      state.authKey = value
    },
    addAccessToken (state, value) {
      state.accessToken = value
    },
    addTerminal (state, value) {
      state.terminal = value
    },
    addTerminalId (state, value) {
      state.terminalId = value
    },
    addOrganizationId (state, value) {
      state.organizationId = value
    },
    addOrganization (state, value) {
      state.organization = value
    },
    addTransactionDescription (state, value) {
      state.transactionDescription = value
    },
    addTransactionDescriptionEditable (state, value) {
      state.transactionDescriptionEditable = !!+value
    },
    deleteAuthKey (state) {
      state.authKey = ''
    },
    deleteAccessToken (state) {
      state.accessToken = ''
    },
    deleteTerminal (state) {
      state.terminal = ''
    },
    deleteTerminalId (state) {
      state.terminalId = ''
    },
    deleteOrganizationId (state) {
      state.organizationId = ''
    },
    deleteOrganization (state) {
      state.organization = ''
    },
    clearAuth (state) {
      state.authKey = ''
      state.accessToken = ''
      state.terminal = ''
      state.terminalId = ''
      state.organizationId = ''
      state.organization = ''
    },
    setCompactMode (state, value) {
      state.compactMode = value
    }
  }
})
