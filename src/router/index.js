import Vue from 'vue'
import VueRouter from 'vue-router'
import Hello from '../components/Hello'
import Scan from '../components/Scan'
import Pin from '../components/Pin'
import Create from '../components/Create'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Hello',
    component: Hello
  },
  {
    path: '/scan',
    name: 'Scan',
    component: Scan
  },
  {
    path: '/pin',
    name: 'Pin',
    component: Pin
  },
  {
    path: '/create',
    name: 'Create',
    component: Create
  }
]

const router = new VueRouter({
  routes
})

export default router
