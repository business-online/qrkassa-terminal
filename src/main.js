import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import axios from './axios'
import '@fortawesome/fontawesome-free/css/all.css'
import Vuelidate from 'vuelidate'
import VueQrcodeReader from 'vue-qrcode-reader'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import vuetify from './plugins/vuetify'
import VuetifyConfirm from 'vuetify-confirm'

Vue.config.productionTip = false

Vue.use(Vuelidate)
Vue.use(VueQrcodeReader)
Vue.use(VuetifyConfirm, { vuetify })

Vue.prototype.$http = axios

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
