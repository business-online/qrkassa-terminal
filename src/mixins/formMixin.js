import { between, decimal, maxLength, required } from 'vuelidate/lib/validators'

export const formMixin = {
  data () {
    return {
      msg: 'Maak Betaal QR Code',
      errorMsg: '',
      amount: '',
      transactionDescription: this.$store.state.transactionDescription,
      transactionDescriptionEditable: this.$store.state.transactionDescriptionEditable,
      oneoff: 1,
      qrcodeUrl: false,
      idqQrcodeUrl: false,
      showPayQr: true,
      idqTransactionID: false,
      idqTransactionStatus: false,
      transactionID: false,
      transactionStatus: false,
      payments: {},
      checks: 0,
      remoteCheck: false,
      terminal: this.$store.state.terminal,
      active: 'eqr',
      es: []
    }
  },
  mounted () {
    if (!this.transactionDescription || this.transactionDescription.length === 0) {
      this.transactionDescriptionEditable = true
    }
  },
  computed: {
    compactMode () {
      return this.$store.state.compactMode
    }
  },
  validations: {
    amount: { required, decimal, between: between(0.50, 1000) },
    transactionDescription: { required, maxLength: maxLength(32) }
  },
  methods: {
    async clear () {
      if (this.transactionID !== false && this.transactionStatus !== 'Completed') {
        let res = await this.$confirm('Weet je zeker dat je de transactie wil annuleren?', {
          title: 'Transactie Annuleren?',
          buttonFalseText: 'Nee',
          buttonTrueText: 'Ja'
        })
        if (res) {
          this.reset()
        } else {
          return false
        }
      }
    },
    reset () {
      this.idqQrcodeUrl = false
      this.qrcodeUrl = false
      this.es = []
      this.active = 'eqr'
      this.transactionID = false
      this.transactionStatus = false
      this.idqTransactionStatus = false
      this.idqTransactionID = false
    },
    formatAmount (value) {
      let val = (value / 1).toFixed(2).replace('.', ',')
      return '€ ' + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    },
    submit () {
      this.$v.amount.$touch()
      this.$v.transactionDescription.$touch()
      if (this.$v.amount.$error) return

      this.create()
    },
    create () {
      const formData = new FormData()
      formData.append('amount', this.amount.toString())
      formData.append('description', this.transactionDescription.toString())
      formData.append('oneoff', this.oneoff.toString())

      this.$http
        .post('/create?access_token=' + this.$store.state.accessToken, formData)
        .then(request => this.created(request))
        .catch((e) => this.failed(e))
    },
    created (r) {
      if (r.error) {
        console.error(r)
        alert(r.error)
        return
      }
      this.qrcodeUrl = (r.data.url ? r.data.url : false)
      this.transactionID = (r.data.id ? r.data.id : false)
      this.initMercure()
    },
    failed (e) {
      this.errorMsg = 'Er is een fout opgetreden, probeer het nogmaals'
      console.error(e)
    },
    createIdq () {
      const formData = new FormData()
      formData.append('transaction_id', this.transactionID.toString())

      this.$http
        .post('/create-idq?access_token=' + this.$store.state.accessToken, formData)
        .then(request => this.idqCreated(request))
        .catch((e) => this.failed(e))
    },

    idqCreated (r) {
      if (r.error) {
        console.error(r)
        alert(r.error)
        return
      }
      this.idqQrcodeUrl = (r.data.url ? r.data.url : false)
      this.idqTransactionID = (r.data.id ? r.data.id : false)
      this.initMercure()
    },
    initMercure () {
      const u = new URL('https://mercure.detechniek.nl/.well-known/mercure')
      const transactionID = (this.active === 'idq' ? this.idqTransactionID : this.transactionID)

      if (!this.es[transactionID]) {
        u.searchParams.append('topic', 'qr-kassa-pwa-transaction-' + transactionID)

        this.es[transactionID] = new EventSource(u)
        this.es[transactionID].onmessage = e => {
          let d = JSON.parse(e.data)
          if (parseInt(d.transaction_id) === parseInt(transactionID)) {
            this.checkTransaction()
          }
        }
      } else {
        alert('error #237')
        console.error('initMercure called with an open connection')
      }
    },
    checkTransaction () {
      this.checks++
      this.remoteCheck = this.checks % 5 === 0
      const transactionID = (this.active === 'idq' ? this.idqTransactionID : this.transactionID)

      if (transactionID === false) {
        this.es[transactionID] = false
        return false
      }

      this.$http
        .get('/get-status?transactionID=' + transactionID + '&remoteCheck=' + (this.remoteCheck ? 1 : 0))
        .then(request => this.processRequest(request, transactionID))
        .catch((e) => this.failed(e))
    },
    processRequest (data, transactionID) {
      if (this.active === 'idq') {
        this.idqTransactionStatus = data.data.status
      } else {
        this.transactionStatus = data.data.status
      }
      if (data.data.payments) {
        this.payments = data.data.payments
      }
      if (data.data.status === 'Completed') {
        this.es[transactionID] = false
      }
    },
    getEqr () {
      // this.showPayQr = true
      // this.active = 'eqr'
    },
    getIdqQr () {
      // this.showPayQr = false
      // this.active = 'idq'

      if (this.idqQrcodeUrl === false || this.idqTransactionID === false) {
        this.createIdq()
      }
    }
  }
}
